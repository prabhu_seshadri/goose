package goose

import (
	"database/sql"

	"github.com/mattn/go-sqlite3"
	"log"
)

// SqlDialect abstracts the details of specific SQL dialects
// for goose's few SQL specific statements
type SqlDialect interface {
	createVersionTableSql(schema string) string // sql string to create the goose_db_version table
	insertVersionSql(schema string) string      // sql string to insert the initial version table row
	dbVersionQuery(db *sql.DB,schema string) (*sql.Rows, error)
}

// drivers that we don't know about can ask for a dialect by name
func dialectByName(d string) SqlDialect {
	switch d {
	case "postgres":
		return &PostgresDialect{}
	case "mysql":
		return &MySqlDialect{}
	case "sqlite3":
		return &Sqlite3Dialect{}
	case "mssql":
		return &MsSqlDialect{}
	}

	return nil
}

////////////////////////////
// Postgres
////////////////////////////

type PostgresDialect struct{}

func (pg PostgresDialect) createVersionTableSql(schema string) string {
	return `CREATE TABLE goose_db_version (
            	id serial NOT NULL,
                version_id bigint NOT NULL,
                is_applied boolean NOT NULL,
                tstamp timestamp NULL default now(),
                PRIMARY KEY(id)
            );`
}

func (pg PostgresDialect) insertVersionSql(schema string) string {
	return "INSERT INTO goose_db_version (version_id, is_applied) VALUES ($1, $2);"
}

func (pg PostgresDialect) dbVersionQuery(db *sql.DB,schema string) (*sql.Rows, error) {
	rows, err := db.Query("SELECT version_id, is_applied from goose_db_version ORDER BY id DESC")

	// XXX: check for postgres specific error indicating the table doesn't exist.
	// for now, assume any error is because the table doesn't exist,
	// in which case we'll try to create it.
	if err != nil {
		return nil, ErrTableDoesNotExist
	}

	return rows, err
}

////////////////////////////
// MySQL
////////////////////////////

type MySqlDialect struct{}

func (m MySqlDialect) createVersionTableSql(schema string) string {
	return `CREATE TABLE `+schema+`.goose_db_version (
                id serial NOT NULL,
                version_id bigint NOT NULL,
                is_applied boolean NOT NULL,
                tstamp timestamp NULL default now(),
                PRIMARY KEY(id)
            );`
}

func (m MySqlDialect) insertVersionSql(schema string) string {
	return "INSERT INTO "+schema+".goose_db_version (version_id, is_applied) VALUES (?, ?);"
}

func (m MySqlDialect) dbVersionQuery(db *sql.DB, schema string) (*sql.Rows, error) {
	rows, err := db.Query("SELECT version_id, is_applied from "+schema+".goose_db_version ORDER BY id DESC")

	// XXX: check for mysql specific error indicating the table doesn't exist.
	// for now, assume any error is because the table doesn't exist,
	// in which case we'll try to create it.
	if err != nil {
		return nil, ErrTableDoesNotExist
	}

	return rows, err
}

////////////////////////////
// sqlite3
////////////////////////////

type Sqlite3Dialect struct{}

func (m Sqlite3Dialect) createVersionTableSql(schema string) string {
	return `CREATE TABLE goose_db_version (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                version_id INTEGER NOT NULL,
                is_applied INTEGER NOT NULL,
                tstamp TIMESTAMP DEFAULT (datetime('now'))
            );`
}

func (m Sqlite3Dialect) insertVersionSql(schema string) string {
	return "INSERT INTO goose_db_version (version_id, is_applied) VALUES (?, ?);"
}

func (m Sqlite3Dialect) dbVersionQuery(db *sql.DB,schema string) (*sql.Rows, error) {
	rows, err := db.Query("SELECT version_id, is_applied from goose_db_version ORDER BY id DESC")

	switch err.(type) {
	case sqlite3.Error:
		return nil, ErrTableDoesNotExist
	}
	return rows, err
}

/////////////////////////
// mssql
////////////////////////

type MsSqlDialect struct{}

func (m MsSqlDialect) createVersionTableSql(schema string) string {
	log.Println()
	return `CREATE TABLE `+schema+`.goose_db_version (
		id INT NOT NULL IDENTITY(1,1)
		, version_id BIGINT NOT NULL
		, is_applied INT NOT NULL
		, tstamp datetimeoffset(7) NULL default GETDATE()
		, PRIMARY KEY(id)
	);`
}

func (m MsSqlDialect) insertVersionSql(schema string) string {
	return "INSERT INTO "+schema+".goose_db_version (version_id, is_applied) VALUES (?, ?);"
}

func (m MsSqlDialect) dbVersionQuery(db *sql.DB,schema string) (*sql.Rows, error) {
	rows, err := db.Query("SELECT version_id, is_applied from "+schema+".goose_db_version ORDER BY id DESC")

	if err != nil {
		return nil, ErrTableDoesNotExist
	}
	return rows, err
}
